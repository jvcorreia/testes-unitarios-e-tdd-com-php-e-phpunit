<?php

namespace OrderBundle\Validators\Test;

use OrderBundle\Validators\NotEmptyValidator;
use PHPUnit\Framework\TestCase;

class NotEmptyValidatorTest extends TestCase{

    //Por convenção, ao usar o PHPUnit usa-se o test no início da função


    /*
        Comando para rodar os testes: ./vendor/bin/phpunit src/OrderBundle/Test/Validators/NotEmptyValidatorTest.php 
    */
    public function testShouldNotBeValidWhenValueIsEmpty()
    {
        $emptyValue = "";
        $notEmptyValidator = new NotEmptyValidator($emptyValue);
        $isValid = $notEmptyValidator->isValid();

        $this->assertFalse($isValid);
    }

    public function testShouldBeValidWhenValueIsNotEmpty()
    {
        $value = 1;
        $notEmptyValidator = new NotEmptyValidator($value);
        $isValid = $notEmptyValidator->isValid();

        $this->assertTrue($isValid);
    }

    /*
        Como podemos ver acima, a função se repete, mas podemos fazer de uma forma em que 
        utilizamos somente uma função, com os data providers (listas com os dados para testar)
    */


    /**
     * @dataProvider valueProvider
     */
    public function testIsValid($value, $expectedResult)
    {
        //coloco como chave o valor que quero passar na função, e como valor o que espero 
        //receber da função

            $notEmptyValidator = new NotEmptyValidator($value);
            $isValid = $notEmptyValidator->isValid();
            $this->assertEquals($isValid, $expectedResult);
        
        
    }

    public function valueProvider()
    {
        return [
            'ShouldBeValidWhenValueIsNotEmpty' => [
                'value' => 'not empty!',
                'expectedResult' => true
            ],
            'ShouldNotBeValidWhenValueIsEmpty' => [
                'value' => '',
                'expectedResult' => false
            ]
        ];
    }

}