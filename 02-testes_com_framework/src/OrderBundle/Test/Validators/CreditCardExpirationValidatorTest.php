<?php

namespace OrderBundle\Validators\Test;

use OrderBundle\Validators\CreditCardExpirationValidator;
use PHPUnit\Framework\TestCase;

class NotEmptyValidatorTest extends TestCase{

    //Por convenção, ao usar o PHPUnit usa-se o test no início da função


    /*
        Comando para rodar os testes: ./vendor/bin/phpunit src/OrderBundle/Test/Validators/CreditCardExpirationValidatorTest.php 
    */
    

    /**
     * @dataProvider valueProvider
     */
    public function testIsValid($value, $expectedResult)
    {
        //coloco como chave o valor que quero passar na função, e como valor o que espero 
        //receber da função

            $CreditCardExpirationDate = new \DateTime($value);
            $CreditCardExpirationValidator = new CreditCardExpirationValidator($CreditCardExpirationDate);
            $isValid = $CreditCardExpirationValidator->isValid();
            $this->assertEquals($isValid, $expectedResult);
        
        
    }

    public function valueProvider()
    {
        return [
            'ShouldBeValidWhenDateIsNotExpired' => [
                'value' => '2021-10-20',
                'expectedResult' => true
            ],
            'ShouldNotBeValidWhenDateIsExpired' => [
                'value' => '2021-02-01',
                'expectedResult' => false
            ],
            'ShouldNotBeValidWhenValueIsTheSameDateNowDate' => [
                'value' => '2021-02-13',
                'expectedResult' => false
            ]
        ];
    }

}