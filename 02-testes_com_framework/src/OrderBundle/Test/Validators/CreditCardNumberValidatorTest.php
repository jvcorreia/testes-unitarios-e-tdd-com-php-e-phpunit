<?php

namespace OrderBundle\Validators\Test;

use OrderBundle\Validators\CreditCardNumberValidator;
use PHPUnit\Framework\TestCase;

class NotEmptyValidatorTest extends TestCase{

    //Por convenção, ao usar o PHPUnit usa-se o test no início da função


    /*
        Comando para rodar os testes: ./vendor/bin/phpunit src/OrderBundle/Test/Validators/CreditCardNumberValidatorTest.php 
    */
    

    /**
     * @dataProvider valueProvider
     */
    public function testIsValid($value, $expectedResult)
    {
        //coloco como chave o valor que quero passar na função, e como valor o que espero 
        //receber da função

            $CreditCardNumberValidator = new CreditCardNumberValidator($value);
            $isValid = $CreditCardNumberValidator->isValid();
            $this->assertEquals($isValid, $expectedResult);
        
        
    }

    public function valueProvider()
    {
        return [
            'ShouldBeValidWhenValueHas16LenghtAndIsNumeric' => [
                'value' => 3324253246234554,
                'expectedResult' => true
            ],
            'ShouldBeValidWhenValueHas16LenghtAndIsNumericString' => [
                'value' => '3324253246234554',
                'expectedResult' => true
            ],
            'ShouldNotBeValidWhenValueHasNot16LenghtAndIsNumericString' => [
                'value' => '24253246234554',
                'expectedResult' => false
            ],
            'ShouldNotBeValidWhenValueHasNot16LenghtAndIsNumeric' => [
                'value' => 24253246234554,
                'expectedResult' => false
            ],
            'ShouldNotBeValidWhenValueHas16LenghtAndIsNotNumeric' => [
                'value' => '123456789012345g',
                'expectedResult' => false
            ]
        ];
    }

}