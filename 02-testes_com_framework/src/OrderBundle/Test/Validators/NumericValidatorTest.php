<?php

namespace OrderBundle\Validators\Test;

use OrderBundle\Validators\NumericValidator;
use PHPUnit\Framework\TestCase;

class NotEmptyValidatorTest extends TestCase{

    //Por convenção, ao usar o PHPUnit usa-se o test no início da função


    /*
        Comando para rodar os testes: ./vendor/bin/phpunit src/OrderBundle/Test/Validators/NumericValidatorTest.php 
    */
    

    /**
     * @dataProvider valueProvider
     */
    public function testIsValid($value, $expectedResult)
    {
        //coloco como chave o valor que quero passar na função, e como valor o que espero 
        //receber da função

            $numericValidator = new NumericValidator($value);
            $isValid = $numericValidator->isValid();
            $this->assertEquals($isValid, $expectedResult);
        
        
    }

    public function valueProvider()
    {
        return [
            'ShouldBeValidWhenValueIsNumeric' => [
                'value' => 3,
                'expectedResult' => true
            ],
            'ShouldNotBeValidWhenValueIsNotNumeric' => [
                'value' => 'a',
                'expectedResult' => false
            ],
            'ShouldBeValidWhenValueIsNumericString' => [
                'value' => '3',
                'expectedResult' => true
            ],
            'ShouldNotBeValidWhenValueIsEmpty' => [
                'value' => '',
                'expectedResult' => false
            ],
        ];
    }

}