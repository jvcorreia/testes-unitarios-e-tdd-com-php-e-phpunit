<?php

class DiscountCalculatorTest {

    public function ShouldApply_WhenValueIsAboveTheMinTest()
    {
        $discountCalculator = new DiscountCalculator();

        $totalValue = 130;
        $totalWithDiscount = $discountCalculator->apply($totalValue);

        $expectedValue = 110;
        $this->assertEquals($totalWithDiscount, $expectedValue);

    }

    public function ShouldNotApply_WhenValueIsAboveTheMinTest()
    {
        $discountCalculator = new DiscountCalculator();

        $totalValue = 60;
        $totalWithDiscount = $discountCalculator->apply($totalValue);

        $expectedValue = 60;
        $this->assertEquals($totalWithDiscount, $expectedValue);

    }

    public function assertEquals($expectedValue, $actualValue)
    {
        if ($expectedValue !== $actualValue) {
            $message = 'Expected: ' . $expectedValue . ' but got ' . $actualValue;
            throw new \Exception($message);
        }

        echo ' Test passed!';
    }
}