<?php

include 'autoloader.php';


/* Forma não recomendada de se fazer
$discountCalTest = new DiscountCalculatorTest();

$discountCalTest->ShouldApply_WhenValueIsAboveTheMinTest();
*/

foreach (new DirectoryIterator(__DIR__) as $file ) { //Iterando os arquivos da pasta

    if (substr($file->getFilename(), -8) !== 'Test.php') { //Achando o arquivo certo
        continue;
    }

    $className = substr($file->getFilename(), 0, -4);
    $testClass = new $className(); //Instanciando a classe de testes

    $methods = get_class_methods($testClass); //pegando todos os métodos

    foreach ($methods as $method) {
        if (substr($method, -4) != 'Test') {
            continue;
        }
        $testClass->$method();
    }

    echo $file->getFilename();
}